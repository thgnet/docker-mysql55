#!/bin/bash

set -eu

mysqlvx="mysql55"

product_file="/srv/builder/products/${mysqlvx}-build.tar.gz"
output_file="${mysqlvx}-build-$(arch).tar.gz"
builder_name="${mysqlvx}builder"

run_output() {
  echo -en "\n\n\e[36;1m*** $1 ***\e[m\n\n\n"
}

# Build the docker image for the builder
run_builder.prepare() {
  run_output "Preparing docker builder image"
  make builder
  echo -e "\n\n*** TAGGED: $(cat builder/.stamp.Dockerfile) ***\n"
}

# Execute a custom builder environment
run_builder.exec() {
  run_output "Starting dev interactive builder"
  make builder/Dockerfile
  (cd builder; docker build -t $builder_name .)
  container_name=""
  exec docker run \
    --name "${builder_name}_dev" \
    --hostname $builder_name.docker \
    --tty \
    -v `pwd`/builder/builder-files:/srv/builder/files \
    -v `pwd`/builder/builder-products:/srv/builder/products \
    -v `pwd`/builder/builder-runtime:/srv/builder/runtime \
    --interactive \
    --rm \
    $builder_name /bin/bash
}

# Run the container and let it do its job
run_builder.build() {
  local image_name=$(cat builder/.stamp.Dockerfile | awk '{ print $1 }')
  run_output "Executing docker builder image '$image_name' as '$container_name'"
  docker run \
    --name $container_name \
    --hostname $builder_name.docker \
    --tty \
    $image_name
}

# Fetch the product of the compilation
run_builder.import() {
  run_output "Extracting products from container '$container_name' as '$output_file'"
  docker cp $container_name:$product_file $output_file
  docker cp $container_name:$product_file.list $output_file.list
  docker cp $container_name:$product_file.manifest $output_file.manifest
  md5sum $output_file > $output_file.md5
}

# Clean up the container
run_builder.delete() {
  run_output "Deleting container '$container_name'"
  docker rm $container_name
}

# Delete the docker image used by the builder
run_builder.clean() {
  local image_name=$(cat builder/.stamp.Dockerfile | awk '{ print $1 }')
  run_output "Removing builder image '$image_name'"
  docker image rm $image_name
}

case "${1:-}" in
all)
  container_name="${builder_name}_$(cat /dev/urandom | tr -cd 'a-z0-9' | head -c 8)"
  run_builder.prepare
  run_builder.build
  run_builder.import
  run_builder.delete
  echo
  echo "Everything went smooth, here is your product:"
  echo
  echo "===> \"$output_file\""
  echo
  echo "You may want to use this product in the Dockerfile"
  echo
  ;;
prepare|clean)
  run_builder.$1
  ;;
build|import|delete)
  if [ "${2:-}" = "" ]
  then
    echo "Usage: $0 $1 <container-suffix>" >&2
    exit 1
  fi
  container_name="${builder_name}_$2"
  run_builder.$1
  ;;
dev)
  run_builder.exec
  ;;
*)
  echo "Usage: $0 [prepare|build|import|delete|all|clean]" >&2
  exit 1
  ;;
esac
