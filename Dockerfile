FROM thgnet/centos7:20240109

LABEL \
  org.label-schema.build-date="2024-01-20T14:00:00Z" \
  org.label-schema.name="MySQL 5.5" \
  org.label-schema.vendor="ThGnet" \
  org.label-schema.version="2.2" \
  org.opencontainers.image.created="2024-01-20 14:00:00+00:00" \
  org.opencontainers.image.title="MySQL 5.5" \
  org.opencontainers.image.vendor="ThGnet"

RUN set -eux; \
  sed -i '/^override_install_langs=/d' /etc/yum.conf; \
  yum -y reinstall glibc-common; \
  yum -y install \
      openssl sudo; \
  pwck -s; rm -f /etc/passwd- /etc/shadow-; \
  grpck -s; rm -f /etc/group- /etc/gshadow-; \
  yum clean all;

ARG XARCH

ADD mysql55-build-$XARCH.tar.gz /

ADD files /

RUN set -eux; \
  groupadd -r mysql; \
  useradd -r -d /var/lib/mysql -g mysql mysql; \
  echo 'export PATH=$PATH:/usr/local/mysql/bin' > /etc/profile.d/mysql.sh; \
  echo $PATH;

VOLUME /var/lib/mysql

EXPOSE 3306

WORKDIR /tmp

ENTRYPOINT [ "docker-entrypoint.sh" ]

CMD [ "mysqld" ]
