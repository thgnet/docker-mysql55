#!/bin/sh
#
# docker-mysql55/builder/builder-files/bin/build-mysql55.sh
#
# Compiles MySQL and generates a binary install package in:
#    /srv/builder/products/mysql55-build.tar.gz
#

set -eu

out() {
  echo
  echo
  echo "[+++] $1"
  echo
  echo
}

filesdir="/srv/builder/files"
workdir="/srv/builder/runtime"
outdir="/srv/builder/products"

srcname="mysql-$BUILDER_MYSQL_VERSION"
srcfile="$srcname.tar.gz"
srcdir="$workdir/$srcname"

outfile="mysql55-build.tar.gz"

cd $workdir

verbose=""
srcstamp=$(cat $filesdir/mysql/$srcfile.stamp)
export SOURCE_DATE_EPOCH=$(date --utc --date="$srcstamp" +%s)

if [ "${1:-}" = "-patch" ]
then
  if [ -e .stamp.builder.1configure -o ! -e .stamp.builder.0unpack ]
  then
    echo "Error: Invalid runtime state, must be unpack" >&2
    exit 1
  fi
  out "Updating source patch file..."
  diff -ur $srcname-orig $srcname | \
    sed -e '/^diff /d; s/^\([+-]\+ .*\)\t[0-9:.+ -]\+/\1/' > $srcname.patch
  if cmp -s $srcname.patch $filesdir/mysql/$srcname.patch
  then
    echo "No need to update the patch file."
  else
    cat $srcname.patch > $filesdir/mysql/$srcname.patch
    echo "Patch file updated."
  fi
  rm -f $srcname.patch
  out "Done."
  exit
fi

if [ "${1:-}" = "-verbose" ]
then
  verbose="1"
  shift
fi

##############################################################################

do_unpack() {
  out "Unpacking source file '$srcfile'..."
  rm -rf $srcname $srcname-orig
  tar -zx --no-same-owner -f $filesdir/mysql/$srcfile
  cp -a $srcname $srcname-orig

  # Apply local patches
  if [ -e "$filesdir/mysql/$srcname.patch" ]
  then
    echo "Applying local patch '$srcname.patch'"
    (cd $srcname; patch -p1 < $filesdir/mysql/$srcname.patch)
  fi

  touch .stamp.builder.0unpack
  rm -f .stamp.builder.1configure
  rm -f .stamp.builder.2build
  rm -f .stamp.builder.3install
}

##############################################################################

do_configure() {
  [ -e .stamp.builder.0unpack ] || do_unpack

  out "Starting configure script inside '$srcname'..."
  pushd $srcdir >/dev/null
  # Reproducible build mangles the debug symbol, but does not help with the
  # problem of the paths order, so we just leave it off
  (set -x; cmake \
    -DCMAKE_C_ARCHIVE_CREATE="<CMAKE_AR> crD <TARGET> <LINK_FLAGS> <OBJECTS>" \
    -DCMAKE_C_ARCHIVE_FINISH="<CMAKE_RANLIB> -D <TARGET>" \
    -DREPRODUCIBLE_BUILD=OFF \
    -DWITH_EMBEDDED_SERVER=OFF \
    -DWITH_UNIT_TESTS=OFF \
    .)
  popd >/dev/null

  touch .stamp.builder.1configure
  rm -f .stamp.builder.2build
  rm -f .stamp.builder.3install
}

##############################################################################

do_build() {
  [ -e .stamp.builder.1configure ] || do_configure

  pushd $srcdir >/dev/null
  if [ -n "$verbose" ]
  then
    out "Starting compilation with single processor..."
    make VERBOSE=1
  else
    local cpus=`grep -c ^processor /proc/cpuinfo`
    out "Starting compilation with $cpus processor/s..."
    make -j $cpus
  fi
  popd >/dev/null

  touch .stamp.builder.2build
  rm -f .stamp.builder.3install
}

##############################################################################

do_install() {
  [ -e .stamp.builder.2build ] || do_build

  pushd $srcdir >/dev/null
  out "Installing..."
  rm -rf /usr/local/mysql
  make install
  # Strip binaries to save (a lot of) space
  (cd /usr/local/mysql/bin; file * | grep 'ELF' | cut -d: -f1 | xargs strip)
  # Remove unwanted stuff
  rm -rf /usr/local/mysql/COPYING
  rm -rf /usr/local/mysql/INSTALL-BINARY
  rm -rf /usr/local/mysql/README
  rm -rf /usr/local/mysql/bin/*test*
  rm -rf /usr/local/mysql/data
  rm -rf /usr/local/mysql/docs
  rm -rf /usr/local/mysql/include
  rm -rf /usr/local/mysql/lib/*.a
  rm -rf /usr/local/mysql/lib/*.so
  rm -rf /usr/local/mysql/lib/plugin/*example*
  rm -rf /usr/local/mysql/lib/plugin/debug
  rm -rf /usr/local/mysql/mysql-test
  rm -rf /usr/local/mysql/sql-bench
  popd >/dev/null

  touch .stamp.builder.3install
}

##############################################################################

do_package() {
  [ -e .stamp.builder.3install ] || do_install

  out "Packaging and publishing..."
  rm -f /tmp/$outfile /tmp/$outfile.list /tmp/$outfile.manifest
  (cd /;
    find \
      usr/local/mysql \
      -print0 | \
    LC_ALL=C sort -z | \
    tar --no-recursion --null -T - -c --numeric-owner \
      --owner=0 --group=0 --mtime=@$SOURCE_DATE_EPOCH | \
    gzip -9nf > /tmp/$outfile)
  (cd /tmp; md5sum $outfile)
  tar -tf /tmp/$outfile > /tmp/$outfile.manifest
  tar -tvf /tmp/$outfile > /tmp/$outfile.list
  mv /tmp/$outfile /tmp/$outfile.list /tmp/$outfile.manifest $outdir
}

##############################################################################

do_clean() {
  out "Cleaning runtime information..."
  rm -rf $srcname $srcname-orig .stamp.builder.*
  rm -rf /usr/local/mysql
}

##############################################################################

do_help() {
  echo "Usage: $0 <target>" >&2
  echo "Targets:" >&2
  echo " - unpack (u)" >&2
  echo " - configure (c)" >&2
  echo " - build (b)" >&2
  echo " - install (i)" >&2
  echo " - package (p)" >&2
  echo " - all (a)" >&2
}

##############################################################################

for action in $@
do
  case "$action" in
    h|help)
      do_help
      ;;
    u|unpack)
      do_unpack
      ;;
    c|conf|configure)
      do_configure
      ;;
    b|build)
      do_build
      ;;
    i|install)
      do_install
      ;;
    p|pkg|package)
      do_package
      ;;
    x|clean)
      do_clean
      ;;
    a|all)
      do_clean
      do_unpack
      do_configure
      do_build
      do_install
      do_package
      ;;
    *)
      echo "Error: Unknown action \"$action\"" >&2
      exit 1
      ;;
  esac
done
