#:TITLE:MySQL 5.5
#:VERSION:2
#:RELEASE:2
#:BUILD:2024-01-20T14:00:00Z
#:TAG:thgnet/mysql55:v@@VERSION@@.@@RELEASE@@
#:TAG:thgnet/mysql55:v@@VERSION@@
#:BUILDER_TAG:thgnet/mysql55builder:v@@VERSION@@
FROM thgnet/centos7:20240109

LABEL \
  org.label-schema.build-date="@@BUILD_DATE_ISO@@" \
  org.label-schema.name="@@TITLE@@" \
  org.label-schema.vendor="ThGnet" \
  org.label-schema.version="@@VERSION@@.@@RELEASE@@" \
  org.opencontainers.image.created="@@BUILD_DATE_OPEN@@" \
  org.opencontainers.image.title="@@TITLE@@" \
  org.opencontainers.image.vendor="ThGnet"

RUN set -eux; \
  # Add extended locale support
  sed -i '/^override_install_langs=/d' /etc/yum.conf; \
  yum -y reinstall glibc-common; \
  # Base environment
  yum -y install \
      openssl sudo; \
  # Cleanup
  pwck -s; rm -f /etc/passwd- /etc/shadow-; \
  grpck -s; rm -f /etc/group- /etc/gshadow-; \
  yum clean all;

%%IF BUILDER -----------------------------------------------------------------
LABEL \
  org.label-schema.name="@@TITLE@@ Builder" \
  org.opencontainers.image.title="@@TITLE@@ Builder"

RUN set -eux; \
  # Build environment
  yum -y install \
    file patch \
    gcc gcc-c++ cmake \
    ncurses-devel \
    bison; \
  echo -en 'export PATH=$PATH:/srv/builder/files/bin\nalias b=build-mysql55.sh\n' > /etc/profile.d/builder.sh; \
  mkdir -p /srv/builder; \
  mkdir -p /srv/builder/products; \
  mkdir -p /srv/builder/runtime;

ENV \
  BUILDER_MYSQL_VERSION="5.5.63+thg0" \
  SOURCE_DATE_EPOCH="@@BUILD_DATE_EPOCH@@"

COPY builder-files /srv/builder/files

WORKDIR /srv/builder/runtime

CMD [ "/srv/builder/files/bin/build-mysql55.sh", "all" ]
%%ENDIF BUILDER --------------------------------------------------------------
%%IF RUNTIME -----------------------------------------------------------------
ARG XARCH

ADD mysql55-build-$XARCH.tar.gz /

ADD files /

RUN set -eux; \
  groupadd -r mysql; \
  useradd -r -d /var/lib/mysql -g mysql mysql; \
  echo 'export PATH=$PATH:/usr/local/mysql/bin' > /etc/profile.d/mysql.sh; \
  echo $PATH;

VOLUME /var/lib/mysql

EXPOSE 3306

WORKDIR /tmp

ENTRYPOINT [ "docker-entrypoint.sh" ]

CMD [ "mysqld" ]
%%ENDIF RUNTIME --------------------------------------------------------------
